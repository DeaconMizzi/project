﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum EnemyState
{
    idle,
    walk,
    attack,
    stagger
}


public class Enemy : MonoBehaviour {

    public EnemyState currentState;
    public FloatValue maxHealth;
    public float health;
    public string enemyName;
    public int baseDamage;
    public float moveSpeed;
    public int enemycount = 9;


    private void Awake()
    {
        health = maxHealth.initialValue;
    }
    
    private void Start()
    {
        health = maxHealth.initialValue;
    }

    private void TakeDamage(float damage)
    {
        health -= damage;
      
        if (health <= 0)
        {
            enemycount = enemycount - 1;
            Destroy(this.gameObject);
            Debug.Log(enemycount);
            
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PlayerMovement.playerhealth -= 1;
        }
    }
    public void Knock(Rigidbody2D myRigidbody, float knocktime, float damage)
    {
        StartCoroutine(KnockCo(myRigidbody, knocktime));
        TakeDamage(damage);
    }

    private IEnumerator KnockCo(Rigidbody2D myRigidbody, float knocktime)
    {
        if (myRigidbody != null)
        {
            yield return new WaitForSeconds(knocktime);
            myRigidbody.velocity = Vector2.zero;
            currentState = EnemyState.idle;
            myRigidbody.velocity = Vector2.zero;
        }
    }

    void Update()
    {
        if (enemycount <= 0)
        {
            SceneManager.LoadScene("Win");
        }
    }

}
